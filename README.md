# SternenStaub2S

A simple [StarTrek](https://de.wikipedia.org/wiki/Star_Trek_(Computerspiel,_1971)) clone.

![Console output of StarTrek](images/Star_Trek_text_game.png)

## Spielablauf 

> Der Spieler steuert ein aus dem Star-Trek-Universum entnommenes Raumschiff Enterprise und versucht Schiffe der feindlichen Klingonen zu zerstören. Mit Hilfe von Textkommandos kann er in und zwischen vierundsechzig Quadranten, (...), navigieren(...) und Waffen benutzen. Aktionen im Spiel kosten Energie, die jedoch in auf dem Spielfeld verteilten Sternenbasen wieder aufgeladen werden kann. [Wikipedia]

Spielziel ist es, alle im Universum befindlichen Klingonen zu zerstören.
## Spielkomponenten
### Universum
Das Universum besteht aus dem Spielfeld mit 64x64 Positionen. Das Spielfeld ist um eine Kugel gewickelt, d.h. x-Position 65 entspricht x-Position 0.
Das Universum merkt sich die Positionen von Spieler(n), Sternen, Klingonen und Basen.

Das Universum erhält Kommandos und reagiert mit Antworten (entweder das Ergebnis einer Aktion oder ein Fehler)

  
Bei der Initialisierung erstellt das Universum das Spielfeld und besetzt es mit einer Zahl von UniversenObjekten vor:
* Sterne
* Klingonen
* Basen
* Spielerschiff
  
Dabei sind die Positionen der einzelnen UniversenObjekten so gewählt, dass sie sich nicht überlappen.

Das Universum versteht folgende Kommandos:
* start
* scan
* move
* shoot
* quit

### Steuerung
Der Spieler oder die Spielerin gibt Textkommandos ein, die der Kommandointerpreter in Kommandos übersetzt. Unbekannte Textkommandos ergeben einen Fehler, ebenso Kommandos mit falscher Zahl von Parametern.
* Help: zeigt alle verfügbaren Kommandos an.
* Start \<name\>: beginnt das Spiel für den Spieler dieses Namens
* Move \<direction>: Bewegt das Schiff um ein Feld in die gewünschte Richtung (n, ne, e, se, s, sw, w, nw)
* Shoot \<direction>: Schießt einen Torpedo in die gewünschte Richtung (n, ne, e, se, s, sw, w, nw). Der Torpedo fliegt eine einstellbare Zahl von Feldern weit
* Quit: beendet das Spiel

##