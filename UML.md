```plantuml
@startuml
title SternenStaub 2S
class SternenStaub2S{
    main()
    begruessung()
    anleitung()
    verabschiedung()
}

class Universum {
    Scan getScan()
}

class Scan {

}
class Spielfeld {

}

class Position {

}

class Kommando {
}

class KommandoInterpreter {
    Kommando getKommando()

}
class Antwort {

}

class UniversenObjekt {

}
class Stern {

}
class Klingone {

}
class Basis {

}
class Spielerschiff {

}
Kommando <|-- StartKommando
Kommando <|-- QuitKommando
Kommando <|-- ScanKommando
Kommando <|-- ShootKommando
Kommando <|-- MoveKommando
SternenStaub2S *- Universum
SternenStaub2S *- KommandoInterpreter
Universum *-- Spielfeld
Spielfeld "n" o-- UniversenObjekt
UniversenObjekt <|-- Stern
UniversenObjekt <|-- Klingone
UniversenObjekt <|-- Spielerschiff
UniversenObjekt <|-- Basis
UniversenObjekt *- Position
@enduml
```