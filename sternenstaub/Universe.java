package sternenstaub;

public class Universe {

    static final int scanWeite = 4;
    static final int anzahlKlingonen = 17;
    static final int anzahlSonnen = 200;

    Spielfeld spielfeld = new Spielfeld();

    SpielerSchiff enterprise;

    public Universe() {
        besetzeSpielfeld();
    }

    public Scan getScan() {
        return new Scan();
    }

    private void besetzeSpielfeld() {
        for (int sonnenNummer = 0; sonnenNummer < anzahlSonnen; sonnenNummer++)
            placeUniverseObject(new Sonne());

        for (int klingonNummer = 0; klingonNummer < anzahlKlingonen; klingonNummer++)
            placeUniverseObject(new Klingone());

        enterprise = new SpielerSchiff();
        placeUniverseObject(enterprise);
    }

    private void placeUniverseObject(UniversenObjekt uo) {
        while (true) {
            Position pos = Position.getRandomPosition();
            if (spielfeld.isFree(pos)) {
                spielfeld.putAt(pos, uo);
                uo.setPosition(pos);
                break;
            }
        }
    }
}
