package sternenstaub;

public class SternenStaub2S {
    public static void main(String[] args) {
        KommandoInterpreter kommandoInterpreter = new KommandoInterpreter();
        Universe universe = new Universe();
        begruessung();
        anleitung();

        Kommando kommando;
        do {
            kommando = kommandoInterpreter.getKommando();
            if (kommando instanceof ScanKommando) {
                Scan scan = universe.getScan();
                String scanText = scan.toString();
                System.out.println(scanText);
            }

        } while (!(kommando instanceof QuitKommando));
        verabschhiedung();
    }

    private static void begruessung() {
        System.out.println("Willkommen bei SternenStaub!");
    }

    private static void anleitung() {
        System.out.println("'quit' zum Beenden");
    }

    private static void verabschhiedung() {
        System.out.println("Danke für's Sternenstaub Spielen");
    }

}
