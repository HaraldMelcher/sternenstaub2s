package sternenstaub;

import java.util.Scanner;

public class KommandoInterpreter {
    Scanner scanner = new Scanner(System.in);

    public Kommando getKommando() {
        System.out.print("Gib Kommando: ");
        String line = scanner.nextLine();
        if (line.startsWith("quit"))
            return new QuitKommando();
        else
            return new MoveKommando();
    }

}
