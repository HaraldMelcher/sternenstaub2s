package sternenstaub;

import java.util.Random;

public class Position {
    static Random rnd = new Random();
    private int x;
    private int y;

    public Position(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public static Position getRandomPosition() {
        int x = rnd.nextInt(64);
        int y = rnd.nextInt(64);
        return new Position(x, y);
    }

    int getX() {
        return x;
    }

    int getY() {
        return y;
    }
}
