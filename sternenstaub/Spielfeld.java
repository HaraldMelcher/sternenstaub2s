package sternenstaub;

public class Spielfeld {

    UniversenObjekt[][] spielfeld = new UniversenObjekt[64][64];

    public void putAt(Position pos, UniversenObjekt uo) {
        spielfeld[pos.getX()][pos.getY()] = uo;
    }

    public boolean isFree(Position pos) {
        return spielfeld[pos.getX()][pos.getY()] == null;
    }
}
